package ru.ws.calculator.client.widget.variants;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import ru.ws.calculator.client.Calculator;
import ru.ws.calculator.shared.CalculatorEngine;
import ru.ws.calculator.shared.variants.Variant12;

import java.math.BigDecimal;
import java.util.Date;

public class Variant12Editor extends VariantEditor<Variant12> {
    interface Variant12EditorUiBinder extends UiBinder<HTMLPanel, Variant12Editor> {}
    private static Variant12EditorUiBinder ourUiBinder = GWT.create(Variant12EditorUiBinder.class);

    interface Driver12 extends SimpleBeanEditorDriver<Variant12, Variant12Editor> {}
    Driver12 driver = GWT.create(Driver12.class);
    @UiField @Path("startPenalty")
    DateBox startOfPenaltyEditor;
    @UiField
    HTMLPanel startOfPenaltyFeedback;
    @UiField @Path("endPenalty")
    DateBox endOfPenaltyEditor;
    @UiField
    HTMLPanel endOfPenaltyFeedback;
    @UiField @Path("contactCost")
    DoubleBox contactCostEditor;
    @UiField
    HTMLPanel contractFeedback;
    @UiField @Path("doneContractCost")
    DoubleBox costOfDoneEditor;
    @UiField
    HTMLPanel costOfDoneFeedback;
    @UiField @Path("penaltyPayDate")
    DateBox dateOfPenaltyEditor;
    @UiField
    HTMLPanel dateOfPenaltyFeedback;
    @UiField
    SpanElement contractCost;
    @UiField
    SpanElement doneCost;
    @UiField
    SpanElement refPay;
    @UiField
    SpanElement multiplier;
    @UiField
    SpanElement daysGap;
    @UiField
    SpanElement totalValue;

    class StartDateEditor implements ValueChangeHandler<Date> {
        @Override
        public void onValueChange(ValueChangeEvent<Date> event) {
            endOfPenaltyEditor.setEnabled(true);
        }
    }

    class EndDateEditor implements ValueChangeHandler<Date> {
        @Override
        public void onValueChange(ValueChangeEvent<Date> event) {
            daysGap.setInnerText("");
            if (event.getValue().before(startOfPenaltyEditor.getValue())) {
                endOfPenaltyEditor.addStyleName("is-invalid");
                endOfPenaltyFeedback.add(new Label("Дата не должна быть раньше начала периода начисления пени"));
                dateOfPenaltyEditor.setEnabled(false);
            } else {
                endOfPenaltyEditor.removeStyleName("is-invalid");
                endOfPenaltyFeedback.clear();
                dateOfPenaltyEditor.setEnabled(true);
                daysGap.setInnerText(String.valueOf(CalendarUtil.getDaysBetween(startOfPenaltyEditor.getValue(), event.getValue())));
            }
        }
    }

    class PenaltyValueEditor implements ValueChangeHandler<Date> {
        @Override
        public void onValueChange(ValueChangeEvent<Date> event) {
            dateOfPenaltyFeedback.clear();
            dateOfPenaltyEditor.removeStyleName("is-invalid");
            refPay.setInnerText("");
            if (event.getValue().before(endOfPenaltyEditor.getValue())) {
                dateOfPenaltyFeedback.add(new Label("Дата уплаты пени не должна быть раньше конца периода начисления пени"));
                dateOfPenaltyEditor.addStyleName("is-invalid");
                return;
            }
            Calculator.rateSelectorServiceAsync.getRateAt(event.getValue(), new AsyncCallback<BigDecimal>() {
                @Override
                public void onFailure(Throwable caught) {
                    dateOfPenaltyFeedback.add(new Label("Ставка не найдена"));
                }

                @Override
                public void onSuccess(BigDecimal result) {
                    if (result == null) {
                        dateOfPenaltyFeedback.add(new Label("Ставка не найдена"));
                        dateOfPenaltyEditor.addStyleName("is-invalid");
                        totalValue.setInnerText("");
                    } else {
                        Variant12 value = getValue();
                        value.setRate(result.doubleValue());
                        edit(value);
                        calculateTotal();
                    }
                }
            });
        }
    }

    private void calculateTotal() {
        Variant12 value = getValue();
        if (value.getContactCost() != null) {
            contractCost.setInnerText(Calculator.format.format(value.getContactCost()));
            BigDecimal contactCost = BigDecimal.valueOf(value.getContactCost());
            if (contactCost.compareTo(new BigDecimal("3000000")) < 1) {
                value.setRate(.1);
            } else if (contactCost.compareTo(new BigDecimal("50000000")) < 1) {
                value.setRate(.05);
            } else if (contactCost.compareTo(new BigDecimal("100000000")) < 1) {
                value.setRate(.01);
            } else if (contactCost.compareTo(new BigDecimal("500000000")) < 1) {
                value.setRate(.005);
            } else if (contactCost.compareTo(new BigDecimal("1000000000")) < 1) {
                value.setRate(.004);
            } else if (contactCost.compareTo(new BigDecimal("2000000000")) < 1) {
                value.setRate(.003);
            } else if (contactCost.compareTo(new BigDecimal("5000000000")) < 1) {
                value.setRate(.0025);
            } else if (contactCost.compareTo(new BigDecimal("10000000000")) < 1) {
                value.setRate(.002);
            } else {
                value.setRate(.001);
            }
        }
        if (value.getDoneContractCost() != null) {
            doneCost.setInnerText(Calculator.format.format(value.getDoneContractCost()));
        }
        if (value.getDivider() != null) {
            multiplier.setInnerText(String.valueOf(value.getDivider()));
        }
        if (value.getRate() != null) {
            refPay.setInnerText(Calculator.format.format(value.getRate()));
        }
        try {
            daysGap.setInnerText(String.valueOf(CalendarUtil.getDaysBetween(value.getStartPenalty(), value.getEndPenalty()) + 2));
        } catch (Throwable e) {}
        try {
            totalValue.setInnerText(Calculator.format.format(CalculatorEngine.calculateVariant12(value).doubleValue()));
        } catch (Throwable e) {}
    }

    public Variant12Editor() {
        initWidget(ourUiBinder.createAndBindUi(this));
        driver.initialize(this);
        Calculator.setupDateBox(startOfPenaltyEditor);
        Calculator.setupDateBox(endOfPenaltyEditor);
        Calculator.setupDateBox(dateOfPenaltyEditor);
        startOfPenaltyFeedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        endOfPenaltyFeedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        dateOfPenaltyFeedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        costOfDoneFeedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        contractFeedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        Scheduler.get().scheduleDeferred(() -> {
            startOfPenaltyEditor.addValueChangeHandler(new StartDateEditor());
            endOfPenaltyEditor.addValueChangeHandler(new EndDateEditor());
            dateOfPenaltyEditor.addValueChangeHandler(new PenaltyValueEditor());
            multiplier.setInnerText(String.valueOf(getValue().getDivider()));
        });
    }

    @Override
    public Variant12 getValue() throws Error {
        return driver.flush();
    }

    @Override
    public void edit(Variant12 value) {
        driver.edit(value);
    }

    @Override
    public boolean isValid() {
        return startOfPenaltyFeedback.getWidgetCount() == 0
                && endOfPenaltyFeedback.getWidgetCount() == 0
                && dateOfPenaltyFeedback.getWidgetCount() == 0
                && costOfDoneFeedback.getWidgetCount() == 0
                && contractFeedback.getWidgetCount() == 0;
    }

    @UiHandler({ "contactCostEditor", "costOfDoneEditor" })
    void contractCostChangeHandler(KeyUpEvent event) {
        calculateTotal();
    }
}