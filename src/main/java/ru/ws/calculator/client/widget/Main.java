package ru.ws.calculator.client.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import ru.ws.calculator.shared.variants.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static ru.ws.calculator.client.Calculator.creatorService;

public class Main extends Composite {
    interface MainUiBinder extends UiBinder<HTMLPanel, Main> {
    }
    private static MainUiBinder ourUiBinder = GWT.create(MainUiBinder.class);

    @UiField
    HTMLPanel calculationsContainer;
    @UiField
    HTMLPanel resultat;
    @UiField
    Button addCalculation;
    @UiField
    Button runCalculation;
    @UiField
    Button clearForm;

    List<CalculationCase> starters = new ArrayList<>();

    public Main() {
        initWidget(ourUiBinder.createAndBindUi(this));
        Scheduler.get().scheduleDeferred(this::addCalculation);
        resultat.getElement().getStyle().setDisplay(Style.Display.BLOCK);
    }
    int calculations = 0;

    @UiHandler("addCalculation")
    void addCalculationEvent(ClickEvent event) {
        addCalculation();
    }

    private void addCalculation() {
        calculations++;
        final CalculationCase e = new CalculationCase();
        e.setHeader("Расчет №" + calculations);
        e.setRemoveHandler(c -> {
            starters.remove(c);
            calculationsContainer.remove(c);
            calculations--;
            int i = 1;
            for (CalculationCase ccase : starters) {
                ccase.setHeader("Расчет №" + i);
                i++;
            }
            if (calculations < 1) {
                addCalculation();
            }
        });
        starters.add(e);
        calculationsContainer.add(e);
        runCalculation.setEnabled(true);
    }

    void setupValue(Serializable value, VariantValue holder) {
        if (value instanceof Variant6) {
            holder.setVariant6((Variant6) value);
            return;
        }
        if (value instanceof Variant7) {
            holder.setVariant7((Variant7) value);
            return;
        }
        if (value instanceof Variant8) {
            holder.setVariant8((Variant8) value);
            return;
        }
        if (value instanceof Variant9) {
            holder.setVariant9((Variant9) value);
            return;
        }
        if (value instanceof Variant10) {
            holder.setVariant10((Variant10) value);
            return;
        }
        if (value instanceof Variant11) {
            holder.setVariant11((Variant11) value);
            return;
        }
        if (value instanceof Variant12) {
            holder.setVariant12((Variant12) value);
            return;
        }
        if (value instanceof Variant13) {
            holder.setVariant13((Variant13) value);
            return;
        }
        if (value instanceof Variant14) {
            holder.setVariant14((Variant14) value);
            return;
        }
    }

    @UiHandler("runCalculation")
    void runCalculation(ClickEvent event) {
        resultat.clear();
        resultat.getElement().removeClassName("invalid-feedback");
        LinkedList<VariantValue> variants = new LinkedList<>();
        for (CalculationCase calcCase : starters) {
            if (calcCase.getVariantEditor().isValid()) {
                VariantValue variantValue = new VariantValue();
                setupValue(calcCase.getVariantEditor().getValue(), variantValue);
                variants.add(variantValue);
            } else {
                resultat.add(new Label("Проверьте правильность заполнения всех полей"));
                resultat.getElement().addClassName("invalid-feedback");
                return;
            }
        }
        creatorService.putData(variants, new AsyncCallback<String>() {
            @Override
            public void onFailure(Throwable caught) {

            }

            @Override
            public void onSuccess(String sessionId) {
                String main_url = GWT.getModuleBaseURL() + "docx;jsessionid=" + sessionId;
                Window.open(main_url, "_blank", "");
            }
        });
        resultat.getElement().setInnerText("");
    }

    @UiHandler("clearForm")
    void clearForm(ClickEvent event) {
        calculationsContainer.clear();
        starters.clear();
        runCalculation.setEnabled(false);
        calculations = 0;
        Scheduler.get().scheduleDeferred(this::addCalculation);
    }

}