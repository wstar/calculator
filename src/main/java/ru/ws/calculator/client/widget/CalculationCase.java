package ru.ws.calculator.client.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.*;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import ru.ws.calculator.client.widget.variants.*;
import ru.ws.calculator.shared.*;
import ru.ws.calculator.shared.variants.*;

import java.util.Arrays;
import java.util.function.Consumer;

public class CalculationCase extends Composite {
    interface CalculationCaseUiBinder extends UiBinder<FormPanel, CalculationCase> { }
    private static CalculationCaseUiBinder ourUiBinder = GWT.create(CalculationCaseUiBinder.class);

    CalculatedData data = new CalculatedData();
    @UiField
    ListBox details;
    @UiField
    HTMLPanel calculationStack;
    @UiField
    HTMLPanel header;

    Consumer<CalculationCase> removeHandler;

    public void setRemoveHandler(Consumer<CalculationCase> removeHandler) {
        this.removeHandler = removeHandler;
    }

    @UiHandler("removeSelf")
    void onRemoveSelfClickHandler(ClickEvent event) {
        if (removeHandler != null) {
            removeHandler.accept(this);
        }
    }

    public void setHeader(String text) {
        header.getElement().removeAllChildren();
        HeadingElement hElement = Document.get().createHElement(5);
        hElement.setInnerText(text);
        header.getElement().appendChild(hElement);
    }

    public CalculationCase() {
        initWidget(ourUiBinder.createAndBindUi(this));
        data.setCallSide(CallSide.PROVIDER);
        data.setPayType(PayType.FINE);
        data.setDetailsOfViolation(ProviderViolation.NONE_OF_ALL);
        setupBootstrapRadioClass(getElement());
        getElement().getStyle().setPaddingBottom(2, Style.Unit.EM);
        Scheduler.get().scheduleDeferred(this::setupDetails);
    }

    public static void setupBootstrapRadioClass(Element element) {
        NodeList<Element> inList = element.getElementsByTagName("input");
        for (int i = 0; i < inList.getLength(); i++) {
            Element item = inList.getItem(i);
            if ("radio".equals(item.getAttribute("type"))) {
                item.addClassName("form-check-input");
                Element following = item.getNextSiblingElement();
                if (following != null && "label".equals(following.getTagName())) {
                    following.setClassName("form-check-label");
                }
                Element previous = item.getPreviousSiblingElement();
                if (previous != null && "label".equals(previous.getTagName())) {
                    previous.setClassName("form-check-label");
                }
            }
        }
    }

    @UiHandler({ "sideOne", "sideTwo" })
    void callSideHandler(ClickEvent event) {
        Object src = event.getSource();
        if (src instanceof RadioButton) {
            RadioButton radio = (RadioButton) src;
            if (radio.getValue()) {
                data.setCallSide(CallSide.valueOf(radio.getFormValue()));
                setupDetails();
            }
        }
    }

    @UiHandler({ "penaltyOne", "penaltyTwo" })
    void payTypeHandler(ClickEvent event) {
        Object src = event.getSource();
        if (src instanceof RadioButton) {
            RadioButton radio = (RadioButton) src;
            if (radio.getValue()) {
                data.setPayType(PayType.valueOf(radio.getFormValue()));
                setupDetails();
            }
        }
    }

    void setupDetails() {
        details.clear();
        if (data.getCallSide() == CallSide.PROVIDER) {
            if (data.getPayType() == PayType.FINE) {
                Arrays.stream(ProviderViolation.values()).forEach(vio -> {
                    details.addItem(vio.getTitle(), vio.getName());
                    details.setItemSelected(vio.ordinal(), data.getDetailsOfViolation() == vio);
                });
            } else if (data.getPayType() == PayType.PENALTY) {
                Arrays.stream(Forfeit.values()).forEach(vio -> {
                    details.addItem(vio.getTitle(), vio.getName());
                    details.setItemSelected(vio.ordinal(), data.getDetailsOfViolation() == vio);
                });
            }
        } else if (data.getCallSide() == CallSide.CUSTOMER) {
            if (data.getPayType() == PayType.FINE) {
                details.addItem("---", (String) null);
            } else if (data.getPayType() == PayType.PENALTY) {
                Arrays.stream(Forfeit.values()).forEach(vio -> {
                    details.addItem(vio.getTitle(), vio.getName());
                    details.setItemSelected(vio.ordinal(), data.getDetailsOfViolation() == vio);
                });
            }
        }
        setupDetails(details);
    }

    VariantEditor variantEditor;

    public VariantEditor getVariantEditor() {
        return variantEditor;
    }

    @UiHandler("details")
    void detailsChangeHandler(ChangeEvent changeEvent) {
        if (variantEditor != null) {
            calculationStack.remove(variantEditor);
        }
        Object src = changeEvent.getSource();
        if (src instanceof ListBox) {
            ListBox box = (ListBox) src;
            setupDetails(box);
        }
    }

    private void setupDetails(ListBox box) {
        if (variantEditor != null) {
            calculationStack.remove(variantEditor);
        }
        String selectedValue = box.getSelectedValue();
        DetailsOfViolation value = null;
        if (data.getPayType() == PayType.FINE && data.getCallSide() == CallSide.CUSTOMER) {
            variantEditor = new Variant13Editor();
            variantEditor.edit(new Variant13());
        } else {
            try {
                value = Forfeit.valueOf(selectedValue);
                switch ((Forfeit) value) {
                    case ONE_130:
                    case ONE_150:
                    case ONE_300: {
                        switch (data.getCallSide()) {
                            case CUSTOMER: {
                                variantEditor = new Variant14Editor();
                                Variant14 value1 = new Variant14();
                                value1.setDivider(((Forfeit) value).getDivider());
                                variantEditor.edit(value1);
                                break;
                            }
                            case PROVIDER: {
                                variantEditor = new Variant12Editor();
                                variantEditor.edit(new Variant12());
                                break;
                            }
                        }
                    }
                }
            } catch (Throwable t) {}
            if (value == null) {
                try {
                    value = ProviderViolation.valueOf(selectedValue);
                    switch ((ProviderViolation) value) {
                        case SMP_SONO: {
                            variantEditor = new Variant6Editor();
                            variantEditor.edit(new Variant6());
                            break;
                        }
                        case WINNER: {
                            variantEditor = new Variant7Editor();
                            variantEditor.edit(new Variant7());
                            break;
                        }
                        case FIRST_VIOLATION: {
                            variantEditor = new Variant8Editor();
                            variantEditor.edit(new Variant8());
                            break;
                        }
                        case SECOND_VIOLATION: {
                            variantEditor = new Variant9Editor();
                            variantEditor.edit(new Variant9());
                            break;
                        }
                        case SECOND_SMP_SONO: {
                            variantEditor = new Variant10Editor();
                            variantEditor.edit(new Variant10());
                            break;
                        }
                        case NONE_OF_ALL: {
                            variantEditor = new Variant11Editor();
                            variantEditor.edit(new Variant11());
                            break;
                        }
                    }
                } catch (Throwable t) {}
            }
            data.setDetailsOfViolation(value);
        }

        calculationStack.add(variantEditor);
        GWT.log(variantEditor.getClass().getName());
    }
}