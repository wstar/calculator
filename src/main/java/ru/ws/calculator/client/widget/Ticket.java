package ru.ws.calculator.client.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;

public class Ticket {
    private final DivElement rootElement;

    interface TicketUiBinder extends UiBinder<DivElement, Ticket> {
    }

    private static TicketUiBinder ourUiBinder = GWT.create(TicketUiBinder.class);
    @UiField
    SpanElement total;
    @UiField
    SpanElement sum;
    @UiField
    SpanElement percent;

    public Ticket(String sum, String percent, String total) {
        rootElement = ourUiBinder.createAndBindUi(this);
        this.total.setInnerText(total);
        this.sum.setInnerText(sum);
        this.percent.setInnerText(percent);
    }

    public DivElement getRootElement() {
        return rootElement;
    }
}