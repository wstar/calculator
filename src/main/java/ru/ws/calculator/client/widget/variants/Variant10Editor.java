package ru.ws.calculator.client.widget.variants;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import ru.ws.calculator.client.Calculator;
import ru.ws.calculator.client.Validators;
import ru.ws.calculator.shared.CalculatorEngine;
import ru.ws.calculator.shared.variants.Variant10;

public class Variant10Editor extends VariantEditor<Variant10> {
    interface Variant10EditorUiBinder extends UiBinder<HTMLPanel, Variant10Editor> { }
    private static Variant10EditorUiBinder ourUiBinder = GWT.create(Variant10EditorUiBinder.class);

    interface Driver10 extends SimpleBeanEditorDriver<Variant10, Variant10Editor> {}
    Driver10 driver = GWT.create(Driver10.class);
    @UiField @Path("contractCost")
    DoubleBox costEditor;
    @UiField
    HTMLPanel feedback;
    @UiField
    SpanElement contractCost;
    @UiField
    SpanElement totalValue;
    @UiField
    SpanElement percent;

    public Variant10Editor() {
        initWidget(ourUiBinder.createAndBindUi(this));
        driver.initialize(this);
        feedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
    }

    @Override
    public Variant10 getValue() throws Error {
        return driver.flush();
    }

    @Override
    public void edit(Variant10 value) {
        driver.edit(value);
    }

    @Override
    public boolean isValid() {
        validate();
        return feedback.getWidgetCount() == 0;
    }

    @UiHandler("costEditor")
    void onKeyUpEvent(KeyUpEvent event) {
        validate();
    }

    private void validate() {
        Validators.validate(costEditor, feedback, v -> {
            Variant10 value = getValue();
            contractCost.setInnerText(Calculator.format.format(value.getContractCost()));
            percent.setInnerText(Calculator.format.format((value.getRate() * 100)));
            totalValue.setInnerText(Calculator.format.format(CalculatorEngine.calculateVariant10(value).doubleValue()));
        });
    }
}