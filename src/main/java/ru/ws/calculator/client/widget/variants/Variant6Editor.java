package ru.ws.calculator.client.widget.variants;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import ru.ws.calculator.client.Calculator;
import ru.ws.calculator.client.Validators;
import ru.ws.calculator.shared.CalculatorEngine;
import ru.ws.calculator.shared.variants.Variant6;

public class Variant6Editor extends VariantEditor<Variant6> {

    interface Variant6EditorUiBinder extends UiBinder<HTMLPanel, Variant6Editor> { }
    private static Variant6EditorUiBinder ourUiBinder = GWT.create(Variant6EditorUiBinder.class);

    interface Driver6 extends SimpleBeanEditorDriver<Variant6, Variant6Editor> {}
    Driver6 driver = GWT.create(Driver6.class);

    @UiField @Path("contractCost")
    DoubleBox costEditor;
    @UiField
    HTMLPanel feedback;
    @UiField
    SpanElement contractCost;
    @UiField
    SpanElement percents;
    @UiField
    SpanElement totalValue;
    @UiField
    SpanElement info;

    public Variant6Editor() {
        initWidget(ourUiBinder.createAndBindUi(this));
        feedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        driver.initialize(this);
        driver.edit(new Variant6());
    }

    @Override
    public void edit(Variant6 value) {
        driver.edit(value);
    }

    @Override
    public boolean isValid() {
        validate();
        return feedback.getWidgetCount() == 0;
    }

    @Override
    public Variant6 getValue() throws Error {
        return driver.flush();
    }

    @UiHandler("costEditor")
    void onKeyUpEvent(KeyUpEvent event) {
        validate();
    }

    private void validate() {
        Validators.validate(costEditor, feedback, v -> {
            Validators.validate_6(costEditor, feedback, vv -> {
                Variant6 value = getValue();
                contractCost.setInnerText(Calculator.format.format(value.getContractCost()));
                percents.setInnerText(Calculator.format.format(value.getRate() * 100));
                double number = CalculatorEngine.calculateVariant6(value).doubleValue();
                String text = "";
                if (number < 1_000) {
                    number = 1_000;
                    text = "Минимальный штраф – 1000 руб";
                } else if (number > 5_000) {
                    number = 5_000;
                    text = "Максимальный штраф – 5 тыс. руб";
                } else {
                    text = "";
                }
                info.setInnerText(text);
                totalValue.setInnerText(Calculator.format.format(number));
            }, 20_000_000.);
        });
    }

}