package ru.ws.calculator.client.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.cellview.client.LoadingStateChangeEvent;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import ru.ws.calculator.client.Calculator;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class RatesAdminDialog extends DialogBox {
    interface RatesAdminPanelUiBinder extends UiBinder<Widget, RatesAdminDialog> {

    }
    private static RatesAdminPanelUiBinder ourUiBinder = GWT.create(RatesAdminPanelUiBinder.class);
    @UiField
    DataGrid<RateEntry> dataTable;
    @UiField
    FlowPanel mainPanel;
    @UiField
    DateBox startDate;
    @UiField
    DoubleBox rateValue;
    @UiField
    Button addRate;
    @UiField
    FormPanel uploadCsvForm;
    @UiField
    FileUpload fileSelector;
    @UiField
    HTMLPanel uploadResponse;

    static class RateEntry {
        Date date;
        BigDecimal rate;

        public RateEntry(Date date, BigDecimal rate) {
            this.date = date;
            this.rate = rate;
        }
    }

    public RatesAdminDialog() {
        setWidget(ourUiBinder.createAndBindUi(this));
        setStyleName("modal modal-dialog-centered modal-sm");
        Calculator.setupDateBox(startDate);
        dataTable.addColumn(new TextColumn<RateEntry>() {
            @Override
            public String getValue(RateEntry entry) {
                return DateTimeFormat.getFormat("dd.MM.yyyy").format(entry.date);
            }
        });
        dataTable.addColumn(new TextColumn<RateEntry>() {
            @Override
            public String getValue(RateEntry entry) {
                return NumberFormat.getDecimalFormat().format(entry.rate);
            }
        });
        dataTable.setSelectionModel(new SingleSelectionModel<RateEntry>());
        dataTable.getSelectionModel().addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                RateEntry entry = ((SingleSelectionModel<RateEntry>) dataTable.getSelectionModel()).getSelectedObject();
                startDate.setValue(entry.date);
                rateValue.setValue(entry.rate.doubleValue());
                addRate.setText("save");
            }
        });
        uploadCsvForm.setAction(GWT.getModuleBaseURL() + "csv-rates");
        uploadCsvForm.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
            @Override
            public void onSubmitComplete(FormPanel.SubmitCompleteEvent event) {
                try {
                    uploadResponse.getElement().setInnerHTML(event.getResults());
                    uploadResponse.getElement().getFirstChildElement().addClassName("text-danger");
                } catch (Throwable e) {}
                loadData();
                uploadCsvForm.reset();
            }
        });
        fileSelector.addChangeHandler(event1 -> {
            uploadResponse.getElement().setInnerHTML("");
            GWT.log(fileSelector.getFilename());
            uploadCsvForm.submit();
        });
    }

    public void show() {
        super.show();
        setVisible(true);
        loadData();
    }

    private void loadData() {
        fireEvent(new LoadingStateChangeEvent(LoadingStateChangeEvent.LoadingState.LOADING));
        Calculator.rateSelectorServiceAsync.getAllRates(new AsyncCallback<Map<Date, BigDecimal>>() {
            @Override
            public void onFailure(Throwable caught) {
                GWT.log("failure", caught);
            }

            @Override
            public void onSuccess(Map<Date, BigDecimal> result) {
                List<RateEntry> rowData = result.entrySet().stream().map(e -> new RateEntry(e.getKey(), e.getValue())).collect(Collectors.toList());
                dataTable.setRowData(rowData);
                dataTable.redraw();
                if (startDate.getValue() != null) {
                    RateEntry selected = rowData.stream().filter(Objects::nonNull).filter(e -> startDate.getValue().equals(e.date)).findFirst().orElse(null);
                    if (selected != null) {
                        dataTable.getSelectionModel().setSelected(selected, true);
                        dataTable.getRowElement(rowData.indexOf(selected)).scrollIntoView();
                    }
                }
                startDate.setValue(null);
                rateValue.setValue(null);
                addRate.setText("add");
            }
        });
    }

    @UiHandler("close")
    void closeHandler(ClickEvent event) {
        hide(true);
    }

    @UiHandler("addRate")
    void addRateHandler(ClickEvent event) {
        Calculator.rateSelectorServiceAsync.addRate(
                startDate.getValue(),
                BigDecimal.valueOf(rateValue.getValue()),
                new AsyncCallback<Void>() {
            @Override
            public void onFailure(Throwable caught) {
                GWT.log("saving rate error", caught);
            }

            @Override
            public void onSuccess(Void result) {
                loadData();
            }
        });
    }

    @UiHandler("removeRate")
    void removeRateHandler(ClickEvent event) {
        Calculator.rateSelectorServiceAsync.removeRate(
                startDate.getValue(),
                new AsyncCallback<Void>() {
                    @Override
                    public void onFailure(Throwable caught) {
                        GWT.log("remove rate error", caught);
                    }

                    @Override
                    public void onSuccess(Void result) {
                        loadData();
                        addRate.setText("add");
                    }
                });
    }

    @UiHandler("downloadCsv")
    void downloadCsvHandler(ClickEvent event) {
        Window.open(GWT.getModuleBaseURL() + "csv-rates", "_blank", "");
    }

    @UiHandler("uploadCsv")
    void uploadCsvHandler(ClickEvent event) {
        fileSelector.click();
    }
}