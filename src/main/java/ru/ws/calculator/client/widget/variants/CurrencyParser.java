package ru.ws.calculator.client.widget.variants;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.text.shared.Parser;

import java.text.ParseException;

public class CurrencyParser implements Parser<Double> {

    private static CurrencyParser INSTANCE;

    /**
     * Returns the instance of the no-op renderer.
     */
    public static Parser<Double> instance() {
        if (INSTANCE == null) {
            INSTANCE = new CurrencyParser();
        }
        return INSTANCE;
    }

    protected CurrencyParser() {
    }

    public Double parse(CharSequence object) throws ParseException {
        if ("".equals(object.toString())) {
            return null;
        }

        try {
            NumberFormat decimalFormat = NumberFormat.getDecimalFormat();
            decimalFormat.overrideFractionDigits(2, 2);
            return decimalFormat.parse(object.toString());
        } catch (NumberFormatException e) {
            throw new ParseException(e.getMessage(), 0);
        }
    }
}

