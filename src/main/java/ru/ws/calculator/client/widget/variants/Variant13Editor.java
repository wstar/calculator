package ru.ws.calculator.client.widget.variants;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import ru.ws.calculator.client.Calculator;
import ru.ws.calculator.client.Validators;
import ru.ws.calculator.shared.CalculatorEngine;
import ru.ws.calculator.shared.variants.Variant13;

import java.math.BigDecimal;

public class Variant13Editor extends VariantEditor<Variant13> {
    interface Variant13EditorUiBinder extends UiBinder<HTMLPanel, Variant13Editor> {}
    private static Variant13EditorUiBinder ourUiBinder = GWT.create(Variant13EditorUiBinder.class);

    interface Driver13 extends SimpleBeanEditorDriver<Variant13, Variant13Editor> {}
    Driver13 driver = GWT.create(Driver13.class);
    @UiField @Path("contractCost")
    DoubleBox costEditor;
    @UiField
    HTMLPanel feedback;
    @UiField
    SpanElement totalValue;

    public Variant13Editor() {
        initWidget(ourUiBinder.createAndBindUi(this));
        driver.initialize(this);
        driver.edit(new Variant13());
        feedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
    }

    @Override
    public Variant13 getValue() throws Error {
        return driver.flush();
    }

    @Override
    public void edit(Variant13 value) {
        driver.edit(value);
    }

    @Override
    public boolean isValid() {
        validate();
        return feedback.getWidgetCount() == 0;
    }

    @UiHandler("costEditor")
    void onKeyUpHandler(KeyUpEvent event) {
        validate();
    }

    private void validate() {
        Validators.validate(costEditor, feedback, v -> {
            Variant13 value = getValue();
            BigDecimal contactCost = BigDecimal.valueOf(value.getContractCost());
            double total = 0;
            if (contactCost.compareTo(new BigDecimal("3000000")) < 1) {
                total =  1_000;
            } else if (contactCost.compareTo(new BigDecimal("50000000")) < 1) {
                total = 5_000;
            } else if (contactCost.compareTo(new BigDecimal("100000000")) < 1) {
                total = 10_000;
            } else {
                total = 100_000;
            }
            Variant13 totalVariantValue = new Variant13();
            totalVariantValue.setContractCost(total);
            totalValue.setInnerText(Calculator.format.format(CalculatorEngine.calculateVariant13(totalVariantValue).doubleValue()));
        });
    }
}