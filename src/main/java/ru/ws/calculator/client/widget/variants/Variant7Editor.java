package ru.ws.calculator.client.widget.variants;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import ru.ws.calculator.client.Calculator;
import ru.ws.calculator.client.Validators;
import ru.ws.calculator.client.widget.CalculationCase;
import ru.ws.calculator.shared.CalculatorEngine;
import ru.ws.calculator.shared.variants.Variant7;

public class Variant7Editor extends VariantEditor<Variant7> {
    interface Variant7EditorUiBinder extends UiBinder<HTMLPanel, Variant7Editor> {}
    private static Variant7EditorUiBinder ourUiBinder = GWT.create(Variant7EditorUiBinder.class);
    @UiField
    HTMLPanel label;
    @UiField @Path("contractCost")
    DoubleBox costEditor;
    @UiField
    HTMLPanel feedback;
    @UiField
    SpanElement contractCost;
    @UiField
    SpanElement totalValue;
    @UiField
    SpanElement percent;

    interface Driver7 extends SimpleBeanEditorDriver<Variant7, Variant7Editor> {}
    Driver7 driver = GWT.create(Driver7.class);

    public Variant7Editor() {
        initWidget(ourUiBinder.createAndBindUi(this));
        CalculationCase.setupBootstrapRadioClass(getElement());
        feedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        driver.initialize(this);
        driver.edit(new Variant7());
    }

    @Override
    public Variant7 getValue() throws Error {
        return driver.flush();
    }

    @Override
    public void edit(Variant7 value) {
        driver.edit(value);
    }

    @Override
    public boolean isValid() {
        validate();
        return feedback.getWidgetCount() == 0;
    }

    @UiHandler({ "highPrice", "lowPrice" })
    void onChange(ClickEvent event) {
        label.clear();
        Object src = event.getSource();
        if (src instanceof RadioButton) {
            RadioButton srcRadio = (RadioButton) src;
            if (srcRadio.getValue()) {
                if ("HIGH".equals(srcRadio.getFormValue())) {
                    label.add(new Label("введите НМЦК"));
                } else if ("LOW".equals(srcRadio.getFormValue())) {
                    label.add(new Label("введите цену контракта"));
                }
            }
        }
    }

    @UiHandler("costEditor")
    void onKeyUp(KeyUpEvent event) {
        validate();
    }

    private void validate() {
        Validators.validate(costEditor, feedback, v -> {
            Validators.validate_with_limit(costEditor, feedback, vv -> {
                Variant7 value = getValue();
                contractCost.setInnerText(Calculator.format.format(value.getContractCost()));
                if (value.getContractCost() <= 3_000_000) {
                    value.setRate(.1);
                } else if (value.getContractCost() <= 50_000_000) {
                    value.setRate(.05);
                } else if (value.getContractCost() <= 100_000_000) {
                    value.setRate(.01);
                }
                percent.setInnerText(Calculator.format.format(value.getRate() * 100));
                totalValue.setInnerText(Calculator.format.format(CalculatorEngine.calculateVariant7(value).doubleValue()));
            }, 100_000_000.);
        });
    }
}