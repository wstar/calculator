package ru.ws.calculator.client.widget.variants;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import ru.ws.calculator.client.Calculator;
import ru.ws.calculator.client.Validators;
import ru.ws.calculator.shared.CalculatorEngine;
import ru.ws.calculator.shared.variants.Variant9;

public class Variant9Editor extends VariantEditor<Variant9> {
    interface Variant9UiBinder extends UiBinder<HTMLPanel, Variant9Editor> {}
    private static Variant9UiBinder ourUiBinder = GWT.create(Variant9UiBinder.class);

    interface Driver9 extends SimpleBeanEditorDriver<Variant9, Variant9Editor> {}
    Driver9 driver = GWT.create(Driver9.class);

    @UiField @Path("contractCost")
    DoubleBox costEditor;
    @UiField
    HTMLPanel feedback;
    @UiField
    SpanElement contractCost;
    @UiField
    SpanElement totalValue;
    @UiField
    SpanElement percent;

    public Variant9Editor() {
        initWidget(ourUiBinder.createAndBindUi(this));
        driver.initialize(this);
        feedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
    }

    @Override
    public Variant9 getValue() throws Error {
        return driver.flush();
    }

    @Override
    public void edit(Variant9 value) {
        driver.edit(value);
    }

    @Override
    public boolean isValid() {
        validate();
        return feedback.getWidgetCount() == 0;
    }

    @UiHandler("costEditor")
    void onCostEdited(KeyUpEvent event) {
        validate();
    }

    private void validate() {
        Validators.validate(costEditor, feedback, v -> {
            Variant9 value = getValue();
            contractCost.setInnerText(Calculator.format.format(value.getContractCost()));
            percent.setInnerText(Calculator.format.format(value.getRate() * 100));
            totalValue.setInnerText(Calculator.format.format(CalculatorEngine.calculateVariant9(value).doubleValue()));
        });
    }
}