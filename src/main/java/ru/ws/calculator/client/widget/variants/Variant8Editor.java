package ru.ws.calculator.client.widget.variants;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import ru.ws.calculator.client.Calculator;
import ru.ws.calculator.client.Validators;
import ru.ws.calculator.shared.CalculatorEngine;
import ru.ws.calculator.shared.variants.Variant8;

public class Variant8Editor extends VariantEditor<Variant8> {
    interface Variant8EditorUiBinder extends UiBinder<HTMLPanel, Variant8Editor> {}
    private static Variant8EditorUiBinder ourUiBinder = GWT.create(Variant8EditorUiBinder.class);

    interface Driver8 extends SimpleBeanEditorDriver<Variant8, Variant8Editor> {}
    Driver8 driver = GWT.create(Driver8.class);

    @UiField @Path("contractCost")
    DoubleBox costEditor;
    @UiField
    HTMLPanel feedback;
    @UiField
    SpanElement totalValue;

    public Variant8Editor() {
        initWidget(ourUiBinder.createAndBindUi(this));
        driver.initialize(this);
        driver.edit(new Variant8());
        driver.flush();
        feedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
    }

    @Override
    public Variant8 getValue() throws Error {
        return driver.flush();
    }

    @Override
    public void edit(Variant8 value) {
        driver.edit(value);
    }

    @Override
    public boolean isValid() {
        validate();
        return feedback.getWidgetCount() == 0;
    }

    @UiHandler("costEditor")
    void onKeyUpHandler(KeyUpEvent event) {
        validate();
    }

    private void validate() {
        Validators.validate(costEditor, feedback, v -> {
            double number = CalculatorEngine.calculateVariant8(getValue()).doubleValue();
            if (number <= 3_000_000) {
                number = 1_000;
            } else if (number <= 50_000_000) {
                number = 5_000;
            } else if (number <= 100_000_000) {
                number = 10_000;
            } else {
                number = 100_000;
            }
            totalValue.setInnerText(Calculator.format.format(number));
        });
    }
}