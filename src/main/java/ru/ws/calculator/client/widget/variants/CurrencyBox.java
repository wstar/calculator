package ru.ws.calculator.client.widget.variants;

import com.google.gwt.dom.client.Document;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.ValueBox;

public class CurrencyBox extends ValueBox<Double> {

    protected CurrencyBox() {
        super(Document.get().createTextInputElement(), CurrencyRenderer.instance(), CurrencyParser.instance());
        addValueChangeHandler(new ValueChangeHandler<Double>() {
            @Override
            public void onValueChange(ValueChangeEvent<Double> event) {

            }
        });
    }


}
