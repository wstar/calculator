package ru.ws.calculator.client.widget.variants;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.Composite;

import java.io.Serializable;

public abstract class VariantEditor<T extends Serializable> extends Composite implements Editor<T> {
    public abstract T getValue() throws Error;
    public abstract void edit(T value);
    public abstract boolean isValid();
}
