package ru.ws.calculator.client.widget.variants;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.text.shared.Renderer;

public class CurrencyRenderer extends AbstractRenderer<Double> {
    private static CurrencyRenderer INSTANCE;

    /**
     * Returns the instance.
     */
    public static Renderer<Double> instance() {
        if (INSTANCE == null) {
            INSTANCE = new CurrencyRenderer();
        }
        return INSTANCE;
    }

    protected CurrencyRenderer() {
    }

    public String render(Double object) {
        if (object == null) {
            return "";
        }

        NumberFormat decimalFormat = NumberFormat.getDecimalFormat();
        decimalFormat.overrideFractionDigits(2, 2);
        return decimalFormat.format(object);
    }
}

