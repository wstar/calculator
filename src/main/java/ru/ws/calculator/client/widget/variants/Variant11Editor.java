package ru.ws.calculator.client.widget.variants;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import ru.ws.calculator.client.Calculator;
import ru.ws.calculator.client.Validators;
import ru.ws.calculator.shared.CalculatorEngine;
import ru.ws.calculator.shared.variants.Variant11;

public class Variant11Editor extends VariantEditor<Variant11> {
    interface Variant11EditorUiBinder extends UiBinder<HTMLPanel, Variant11Editor> {}
    private static Variant11EditorUiBinder ourUiBinder = GWT.create(Variant11EditorUiBinder.class);

    interface Driver11 extends SimpleBeanEditorDriver<Variant11, Variant11Editor> {}
    Driver11 driver = GWT.create(Driver11.class);
    @UiField @Path("contractCost")
    DoubleBox costEditor;
    @UiField
    HTMLPanel feedback;
    @UiField
    SpanElement totalValue;
    @UiField
    SpanElement percent;
    @UiField
    SpanElement contractCost;

    public Variant11Editor() {
        initWidget(ourUiBinder.createAndBindUi(this));
        driver.initialize(this);
        feedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
    }

    @Override
    public Variant11 getValue() throws Error {
        return driver.flush();
    }

    @Override
    public void edit(Variant11 value) {
        driver.edit(value);
    }

    @Override
    public boolean isValid() {
        validate();
        return feedback.getWidgetCount() == 0;
    }

    @UiHandler("costEditor")
    void onKeyUpEvent(KeyUpEvent event) {
        validate();
    }

    private void validate() {
        Validators.validate(costEditor, feedback, v -> {
            Validators.validate_with_limit(costEditor, feedback, vv -> {
                Variant11 value = getValue();
                contractCost.setInnerText(Calculator.format.format(value.getContractCost()));
                percent.setInnerText(Calculator.format.format(value.getRate() * 100));
                totalValue.setInnerText(Calculator.format.format(CalculatorEngine.calculateVariant11(value).doubleValue()));
            }, 20_000_000.);
        });
    }
}