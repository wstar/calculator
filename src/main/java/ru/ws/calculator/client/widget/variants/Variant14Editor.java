package ru.ws.calculator.client.widget.variants;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;
import ru.ws.calculator.client.Calculator;
import ru.ws.calculator.client.Validators;
import ru.ws.calculator.shared.CalculatorEngine;
import ru.ws.calculator.shared.variants.Variant14;

import java.math.BigDecimal;
import java.util.Date;

public class Variant14Editor extends VariantEditor<Variant14> {

    interface Variant14EditorUiBinder extends UiBinder<HTMLPanel, Variant14Editor> {}
    private static Variant14EditorUiBinder ourUiBinder = GWT.create(Variant14EditorUiBinder.class);

    interface Driver14 extends SimpleBeanEditorDriver<Variant14, Variant14Editor> {}
    Driver14 driver = GWT.create(Driver14.class);
    @UiField @Path("penaltyPayDate")
    DateBox dateOfPenaltyEditor;
    @UiField @Path("startPenalty")
    DateBox startOfPenaltyEditor;
    @UiField
    HTMLPanel startOfPenaltyFeedback;
    @UiField @Path("endPenalty")
    DateBox endOfPenaltyEditor;
    @UiField
    HTMLPanel endOfPenaltyFeedback;
    @UiField @Path("contactCost")
    DoubleBox contactCostEditor;
    @UiField @Path("doneContractCost")
    DoubleBox costOfDoneEditor;
    @UiField
    HTMLPanel costOfDoneFeedback;
    @UiField
    HTMLPanel contractFeedback;
    @UiField
    SpanElement contractCost;
    @UiField
    SpanElement doneCost;
    @UiField
    SpanElement refPay;
    @UiField
    SpanElement multiplier;
    @UiField
    SpanElement daysGap;
    @UiField
    SpanElement totalValue;
    @UiField
    HTMLPanel dateOfPenaltyFeedback;

    class StartDateEditor implements ValueChangeHandler<Date> {
        @Override
        public void onValueChange(ValueChangeEvent<Date> event) {
            endOfPenaltyEditor.setEnabled(true);
        }
    }

    class EndDateEditor implements ValueChangeHandler<Date> {
        @Override
        public void onValueChange(ValueChangeEvent<Date> event) {
            daysGap.setInnerText("");
            if (event.getValue().before(startOfPenaltyEditor.getValue())) {
                endOfPenaltyEditor.addStyleName("is-invalid");
                endOfPenaltyFeedback.add(new Label("Дата не должна быть раньше начала периода начисления пени"));
                dateOfPenaltyEditor.setEnabled(false);
            } else {
                endOfPenaltyEditor.removeStyleName("is-invalid");
                endOfPenaltyFeedback.clear();
                dateOfPenaltyEditor.setEnabled(true);
                daysGap.setInnerText(String.valueOf(CalendarUtil.getDaysBetween(startOfPenaltyEditor.getValue(), event.getValue())));
            }
        }
    }

    class PenaltyValueEditor implements ValueChangeHandler<Date> {
        @Override
        public void onValueChange(ValueChangeEvent<Date> event) {
            dateOfPenaltyFeedback.clear();
            dateOfPenaltyEditor.removeStyleName("is-invalid");
            refPay.setInnerText("");
            if (event.getValue().before(endOfPenaltyEditor.getValue())) {
                dateOfPenaltyFeedback.add(new Label("Дата уплаты пени не должна быть раньше конца периода начисления пени"));
                dateOfPenaltyEditor.addStyleName("is-invalid");
                return;
            }
            Calculator.rateSelectorServiceAsync.getRateAt(event.getValue(), new AsyncCallback<BigDecimal>() {
                @Override
                public void onFailure(Throwable caught) {
                    dateOfPenaltyFeedback.add(new Label("Ставка не найдена"));
                }

                @Override
                public void onSuccess(BigDecimal result) {
                    if (result == null) {
                        dateOfPenaltyFeedback.add(new Label("Ставка не найдена"));
                        dateOfPenaltyEditor.addStyleName("is-invalid");
                        totalValue.setInnerText("");
                    } else {
                        Variant14 value = getValue();
                        value.setRate(result.doubleValue());
                        edit(value);
                        calculateTotal();
                    }
                }
            });
        }
    }

    void calculateTotal() {
        Variant14 value = getValue();
        if (value.getContactCost() != null) {
            contractCost.setInnerText(Calculator.format.format(value.getContactCost()));
        }
        if (value.getDoneContractCost() != null) {
            doneCost.setInnerText(Calculator.format.format(value.getDoneContractCost()));
        }
        if (value.getDivider() != null) {
            multiplier.setInnerText(String.valueOf(value.getDivider()));
        }
        if (value.getRate() != null) {
            refPay.setInnerText(Calculator.format.format(value.getRate()));
        }
        try {
            daysGap.setInnerText(String.valueOf(CalendarUtil.getDaysBetween(value.getStartPenalty(), value.getEndPenalty()) + 2));
        } catch (Throwable e) {}
        try {
            totalValue.setInnerText(Calculator.format.format(CalculatorEngine.calculateVariant14(value).doubleValue()));
        } catch (Throwable e) {}
    }

    public Variant14Editor() {
        initWidget(ourUiBinder.createAndBindUi(this));
        driver.initialize(this);
        startOfPenaltyFeedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        endOfPenaltyFeedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        dateOfPenaltyFeedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        costOfDoneFeedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        contractFeedback.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        Calculator.setupDateBox(startOfPenaltyEditor);
        Calculator.setupDateBox(endOfPenaltyEditor);
        Calculator.setupDateBox(dateOfPenaltyEditor);
        Scheduler.get().scheduleDeferred(() -> {
            startOfPenaltyEditor.addValueChangeHandler(new StartDateEditor());
            endOfPenaltyEditor.addValueChangeHandler(new EndDateEditor());
            dateOfPenaltyEditor.addValueChangeHandler(new PenaltyValueEditor());
            multiplier.setInnerText(String.valueOf(getValue().getDivider()));
        });

    }

    @Override
    public Variant14 getValue() throws Error {
        return driver.flush();
    }

    @Override
    public void edit(Variant14 value) {
        driver.edit(value);
    }

    @Override
    public boolean isValid() {
        validate();
        return startOfPenaltyFeedback.getWidgetCount() == 0
                && endOfPenaltyFeedback.getWidgetCount() == 0
                && dateOfPenaltyFeedback.getWidgetCount() == 0
                && costOfDoneFeedback.getWidgetCount() == 0
                && contractFeedback.getWidgetCount() == 0;
    }

    @UiHandler({ "contactCostEditor", "costOfDoneEditor" })
    void contractCostChangeHandler(KeyUpEvent event) {
        validate();
    }

    void validate() {
        Validators.validate(contactCostEditor, contractFeedback, v -> {

        });
        Validators.validate(costOfDoneEditor, costOfDoneFeedback, v -> {

        });
        try {
            if (costOfDoneEditor.getValue() > contactCostEditor.getValue()) {
                costOfDoneEditor.addStyleName("is-invalid");
                costOfDoneFeedback.add(new Label("Исполненная часть контракта не должна превышать цену контракта"));
            } else {
                costOfDoneEditor.removeStyleName("is-invalid");
                costOfDoneFeedback.clear();
                calculateTotal();
            }
        } catch (Throwable t) {}
    }
}