package ru.ws.calculator.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;

import java.util.function.Consumer;

public class Validators {
    public static void validate(DoubleBox editor, HTMLPanel feedback, Consumer<Double> onEdit) {
        feedback.clear();
        Double value = 0.0;

        try {
            if (!editor.getText().isEmpty()) {
                value = editor.getValueOrThrow();
            }
        } catch (Throwable t) {
            GWT.log("err1", t);
            feedback.add(new Label("Только цифры."));
            return;
        }

        if (value == 0) {
            feedback.add(new Label("Укажите цену контракта (этапа)"));
            return;
        }
        if (value < 0) {
            feedback.add(new Label("Значение должно быть положительным"));
        }
        if (onEdit != null) {
            onEdit.accept(value);
        }
        if (feedback.getWidgetCount() > 0) {
            editor.addStyleName("is-invalid");
        } else {
            editor.removeStyleName("is-invalid");
        }
    }

    public static void validate_with_limit(DoubleBox editor, HTMLPanel feedback, Consumer<Double> onEdit, Double limit) {
        feedback.clear();
        Double value = 0.0;

        try {
            if (!editor.getText().isEmpty()) {
                value = editor.getValueOrThrow();
            }
        } catch (Throwable t) {
            GWT.log("err1", t);
            feedback.add(new Label("Только цифры."));
            return;
        }

        if (value > limit) {
            feedback.add(new Label("Не более " + Calculator.format.format(limit)));
        }
        if (onEdit != null) {
            onEdit.accept(value);
        }
        if (feedback.getWidgetCount() > 0) {
            editor.addStyleName("is-invalid");
        } else {
            editor.removeStyleName("is-invalid");
        }
    }

    public static void validate_6(DoubleBox costEditor, HTMLPanel feedback, Consumer<Double> onEdit, double v) {
        validate_with_limit(costEditor, feedback, onEdit, v);

    }
}
