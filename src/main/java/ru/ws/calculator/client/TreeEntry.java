package ru.ws.calculator.client;

import com.google.gwt.dom.client.*;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;

import java.util.HashMap;
import java.util.Map;

public class TreeEntry {
    protected String title;
    protected HashMap<String, TreeEntry> values = new HashMap<>();
    private DivElement row;
    private DivElement valueHolder;
    private String selectedEntryName = null;

    public TreeEntry() {
        this.title = "";
    }

    public TreeEntry(String title, DivElement holder) {
        this.title = title;
        this.valueHolder = holder;
    }

    TreeEntry add(String title, TreeEntry entry){
        values.put(title, entry);
        return this;
    }

    TreeEntry add(String title, boolean selected, TreeEntry entry){
        add(title, entry);
        if (selected) {
            selectedEntryName = title;
        }
        return this;
    }

    DivElement getRow() {
        row = Document.get().createDivElement();
        row.setClassName("row");
        row.appendChild(getTitle());
        row.appendChild(getSelector());
        return row;
    }

    protected Node getSelector() {
        DivElement row = Document.get().createDivElement();
        row.setClassName("col");
        DivElement group = Document.get().createDivElement();
        group.addClassName("form-group");
        Map.Entry<String, TreeEntry>[] array = values.entrySet().toArray(new Map.Entry[0]);
        for (int i = 0; i < array.length; i++) {
            Map.Entry<String, TreeEntry> e = array[i];
            DivElement formCheck = Document.get().createDivElement();
            formCheck.setClassName("form-check");

            String name = "group" + String.valueOf(title.hashCode());
            InputElement input = Document.get().createRadioInputElement(name);
            input.setClassName("form-check-input");
            input.setValue(name + i);
            input.setId(name + i);

            setupOnClick(input, e.getValue());
            formCheck.appendChild(input);
            if (e.getKey().equals(selectedEntryName)) {
                input.setAttribute("selected", "true");
                input.select();
            }

            LabelElement label = Document.get().createLabelElement();
            label.setClassName("form-check-label");
            label.setHtmlFor(name + i);
            label.setInnerText(e.getKey());
            formCheck.appendChild(label);

            group.appendChild(formCheck);
        }
        row.appendChild(group);
        return row;
    }

    private void setupOnClick(Element e, final TreeEntry entry) {
        DOM.sinkEvents(e, Event.ONCHANGE);
        DOM.setEventListener(e, event -> {
            if (Event.ONCHANGE == event.getTypeInt() && valueHolder != null) {
                valueHolder.removeAllChildren();
                valueHolder.appendChild(entry.getRow());
                entry.reset();
                selectedEntryName = values.entrySet().stream().filter(entry::equals).findFirst().get().getKey();
            }
        });
    }

    private void reset() {
        valueHolder.removeAllChildren();
    }

    protected Node getTitle() {
        DivElement row = Document.get().createDivElement();
        row.setClassName("col");
        row.setInnerText(title);
        return row;
    }
}
