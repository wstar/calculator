package ru.ws.calculator.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import ru.ws.calculator.client.widget.Main;
import ru.ws.calculator.client.widget.RatesAdminDialog;
import ru.ws.calculator.shared.RateSelectorService;
import ru.ws.calculator.shared.RateSelectorServiceAsync;
import ru.ws.calculator.shared.TemplateCreatorService;
import ru.ws.calculator.shared.TemplateCreatorServiceAsync;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Calculator implements EntryPoint {

    static private final String startSelector = "fz44-calculator";
    static private final String ratesAdminBtn = "calculator-rates-admin-open-dlg-btn";

    public static void setupDateBox(DateBox box) {
        box.getDatePicker().setYearAndMonthDropdownVisible(true);
        box.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd.MM.yyyy")));
    }

    public static final TemplateCreatorServiceAsync creatorService = (TemplateCreatorServiceAsync) GWT.create(TemplateCreatorService.class);
    public static final RateSelectorServiceAsync rateSelectorServiceAsync = (RateSelectorServiceAsync) GWT.create(RateSelectorService.class);
    public static NumberFormat format = NumberFormat.getFormat("#,##0.00");

    public void onModuleLoad() {
        RootPanel.get(startSelector).add(new Main());
        RootPanel ratesBtn = RootPanel.get(ratesAdminBtn);
        if (ratesBtn != null) {
            Button.wrap(ratesBtn.getElement()).addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    new RatesAdminDialog().show();
                }
            });
        }

    }
}
