package ru.ws.calculator.shared;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import ru.ws.calculator.shared.variants.VariantValue;

import java.util.List;

@RemoteServiceRelativePath("TemplateCreatorService")
public interface TemplateCreatorService extends RemoteService {
    String putData(List<VariantValue> value);
}
