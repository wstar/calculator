package ru.ws.calculator.shared;

public enum PayType {
    FINE, PENALTY;
}
