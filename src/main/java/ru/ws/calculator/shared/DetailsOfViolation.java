package ru.ws.calculator.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;

public interface DetailsOfViolation extends Serializable, IsSerializable {
    String getTitle();
    String getName();

    DetailsOfViolation forValue(String value);
}
