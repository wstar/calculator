package ru.ws.calculator.shared;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

public interface RateSelectorServiceAsync {
    void getRateAt(Date date, AsyncCallback<BigDecimal> async);

    void getAllRates(AsyncCallback<Map<Date, BigDecimal>> async);

    void addRate(Date date, BigDecimal rate, AsyncCallback<Void> async);

    void removeRate(Date date, AsyncCallback<Void> async);
}
