package ru.ws.calculator.shared;

public enum ProviderViolation implements DetailsOfViolation {
    SMP_SONO("Поставщик - СМП, СОНО"),
    WINNER("Поставщик - победитель закупки на право заключить контракт"),
    FIRST_VIOLATION("Поставщик нарушил обязательство без стоимостного выражения"),
    SECOND_VIOLATION("Подрядчик ненадлежаще выполнил работы по строительству, реконструкции, которые обязан выполнить самостоятельно"),
    SECOND_SMP_SONO("Подрядчик в нарушение контракта не привлек субподрядчиков из СМП и СОНО"),
    NONE_OF_ALL("Нет перечисленных особенностей")
    ;

    final String title;

    ProviderViolation(String title) {
        this.title = title;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public DetailsOfViolation forValue(String value) {
        return valueOf(value);
    }
}
