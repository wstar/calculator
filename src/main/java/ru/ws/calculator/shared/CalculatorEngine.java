package ru.ws.calculator.shared;

import com.google.gwt.user.datepicker.client.CalendarUtil;
import ru.ws.calculator.shared.variants.*;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CalculatorEngine {
    public static BigDecimal calculateVariant6(Variant6 value) {
        return BigDecimal.valueOf(value.getContractCost())
                .multiply(BigDecimal.valueOf(value.getRate()))
                .setScale(2, RoundingMode.HALF_UP);
    }

    public static BigDecimal calculateVariant7(Variant7 value) {
        return BigDecimal.valueOf(value.getContractCost())
                .multiply(BigDecimal.valueOf(value.getRate()))
                .setScale(2, RoundingMode.HALF_UP);
    }

    public static BigDecimal calculateVariant8(Variant8 value) {
        return BigDecimal.valueOf(value.getContractCost())
                .setScale(2, RoundingMode.HALF_UP);
    }

    public static BigDecimal calculateVariant9(Variant9 value) {
        return BigDecimal.valueOf(value.getContractCost())
                .multiply(BigDecimal.valueOf(value.getRate()))
                .setScale(2, RoundingMode.HALF_UP);
    }

    public static BigDecimal calculateVariant10(Variant10 value) {
        return BigDecimal.valueOf(value.getContractCost())
                .multiply(BigDecimal.valueOf(value.getRate()))
                .setScale(2, RoundingMode.HALF_UP);
    }

    public static BigDecimal calculateVariant11(Variant11 value) {
        return BigDecimal.valueOf(value.getContractCost())
                .multiply(BigDecimal.valueOf(value.getRate()))
                .setScale(2, RoundingMode.HALF_UP);
    }

    public static BigDecimal calculateVariant12(Variant12 value) {
        return BigDecimal.valueOf(value.getContactCost())
                .subtract(BigDecimal.valueOf(value.getDoneContractCost()))
                .multiply(BigDecimal.valueOf(CalendarUtil.getDaysBetween(value.getStartPenalty(), value.getEndPenalty()) + 2))
                .multiply(BigDecimal.valueOf(value.getRate()).divide(BigDecimal.valueOf(100), 5, RoundingMode.HALF_UP))
                .divide(BigDecimal.valueOf(value.getDivider()), 2, RoundingMode.HALF_UP);
    }

    public static BigDecimal calculateVariant13(Variant13 value) {
        return BigDecimal.valueOf(value.getContractCost()).setScale(2, RoundingMode.HALF_UP);
    }

    public static BigDecimal calculateVariant14(Variant14 value) {
        return BigDecimal.valueOf(value.getContactCost())
                .subtract(BigDecimal.valueOf(value.getDoneContractCost()))
                .multiply(BigDecimal.valueOf(CalendarUtil.getDaysBetween(value.getStartPenalty(), value.getEndPenalty()) + 2))
                .multiply(BigDecimal.valueOf(value.getRate()).divide(BigDecimal.valueOf(100), 5, RoundingMode.HALF_UP))
                .divide(BigDecimal.valueOf(value.getDivider()), 2, RoundingMode.HALF_UP);
    }
}
