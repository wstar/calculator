package ru.ws.calculator.shared;

import ru.ws.calculator.shared.variants.VariantValue;

import java.io.Serializable;

public class CalculatedData implements Serializable {
    CallSide callSide;
    PayType payType;
    DetailsOfViolation detailsOfViolation;

    String name;
    Integer value;
    private VariantValue variantValue;

    public DetailsOfViolation getDetailsOfViolation() {
        return detailsOfViolation;
    }

    public void setDetailsOfViolation(DetailsOfViolation detailsOfViolation) {
        this.detailsOfViolation = detailsOfViolation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public CallSide getCallSide() {
        return callSide;
    }

    public void setCallSide(CallSide callSide) {
        this.callSide = callSide;
    }

    public PayType getPayType() {
        return payType;
    }

    public void setPayType(PayType payType) {
        this.payType = payType;
    }

    @Override
    public String toString() {
        return "CalculatedData{" +
                "callSide=" + callSide +
                ", payType=" + payType +
                ", detailsOfViolation=" + detailsOfViolation +
                ", name='" + name + '\'' +
                ", value=" + value +
                ", variantValue=" + variantValue +
                '}';
    }

    public void setVariantValue(VariantValue variantValue) {
        this.variantValue = variantValue;
    }

    public VariantValue getVariantValue() {
        return variantValue;
    }
}
