package ru.ws.calculator.shared;

import com.google.gwt.user.client.rpc.AsyncCallback;
import ru.ws.calculator.shared.variants.VariantValue;

import java.util.List;

public interface TemplateCreatorServiceAsync {
    void putData(List<VariantValue> value, AsyncCallback<String> async);
}
