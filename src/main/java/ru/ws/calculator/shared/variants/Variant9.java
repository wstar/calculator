package ru.ws.calculator.shared.variants;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;

public class Variant9 implements Serializable, IsSerializable {
    Double contractCost;
    private Double rate = .05;

    public Double getContractCost() {
        return contractCost;
    }

    public void setContractCost(Double contractCost) {
        this.contractCost = contractCost;
    }

    @Override
    public String toString() {
        return "Variant9{" +
                "contractCost=" + contractCost +
                '}';
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }
}
