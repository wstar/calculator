package ru.ws.calculator.shared.variants;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;

public class Variant13 implements Serializable, IsSerializable {
    Double contractCost;

    public Double getContractCost() {
        return contractCost;
    }

    public void setContractCost(Double contractCost) {
        this.contractCost = contractCost;
    }

    @Override
    public String toString() {
        return "Variant13{" +
                "contractCost=" + contractCost +
                '}';
    }
}
