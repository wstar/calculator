package ru.ws.calculator.shared.variants;

import com.google.gwt.user.client.rpc.IsSerializable;
import ru.ws.calculator.shared.Forfeit;

import java.io.Serializable;
import java.util.Date;

public class Variant12 implements Serializable, IsSerializable {
    Double contactCost;
    Double doneContractCost;
    Date penaltyPayDate;
    Date startPenalty;
    Date endPenalty;
    Integer divider = Forfeit.ONE_300.getDivider();
    Double rate;

    public Integer getDivider() {
        return divider;
    }

    public void setDivider(Integer divider) {
        this.divider = divider;
    }

    public Double getContactCost() {
        return contactCost;
    }

    public void setContactCost(Double contactCost) {
        this.contactCost = contactCost;
    }

    public Double getDoneContractCost() {
        return doneContractCost;
    }

    public void setDoneContractCost(Double doneContractCost) {
        this.doneContractCost = doneContractCost;
    }

    public Date getPenaltyPayDate() {
        return penaltyPayDate;
    }

    public void setPenaltyPayDate(Date penaltyPayDate) {
        this.penaltyPayDate = penaltyPayDate;
    }

    public Date getStartPenalty() {
        return startPenalty;
    }

    public void setStartPenalty(Date startPenalty) {
        this.startPenalty = startPenalty;
    }

    public Date getEndPenalty() {
        return endPenalty;
    }

    public void setEndPenalty(Date endPenalty) {
        this.endPenalty = endPenalty;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }
}
