package ru.ws.calculator.shared.variants;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;

public class VariantValue implements Serializable, IsSerializable {
    Variant6 variant6;
    Variant7 variant7;
    Variant8 variant8;
    Variant9 variant9;
    Variant10 variant10;
    Variant11 variant11;
    Variant12 variant12;
    Variant13 variant13;
    Variant14 variant14;

    public Variant6 getVariant6() {
        return variant6;
    }

    public void setVariant6(Variant6 variant6) {
        this.variant6 = variant6;
    }

    public Variant7 getVariant7() {
        return variant7;
    }

    public void setVariant7(Variant7 variant7) {
        this.variant7 = variant7;
    }

    public Variant8 getVariant8() {
        return variant8;
    }

    public void setVariant8(Variant8 variant8) {
        this.variant8 = variant8;
    }

    public Variant9 getVariant9() {
        return variant9;
    }

    public void setVariant9(Variant9 variant9) {
        this.variant9 = variant9;
    }

    public Variant10 getVariant10() {
        return variant10;
    }

    public void setVariant10(Variant10 variant10) {
        this.variant10 = variant10;
    }

    public Variant11 getVariant11() {
        return variant11;
    }

    public void setVariant11(Variant11 variant11) {
        this.variant11 = variant11;
    }

    public Variant12 getVariant12() {
        return variant12;
    }

    public void setVariant12(Variant12 variant12) {
        this.variant12 = variant12;
    }

    public Variant13 getVariant13() {
        return variant13;
    }

    public void setVariant13(Variant13 variant13) {
        this.variant13 = variant13;
    }

    public Variant14 getVariant14() {
        return variant14;
    }

    public void setVariant14(Variant14 variant14) {
        this.variant14 = variant14;
    }

    @Override
    public String toString() {
        return "VariantValue{" +
                "variant6=" + variant6 +
                ", variant7=" + variant7 +
                ", variant8=" + variant8 +
                ", variant9=" + variant9 +
                ", variant10=" + variant10 +
                ", variant12=" + variant12 +
                ", variant13=" + variant13 +
                ", variant14=" + variant14 +
                '}';
    }
}
