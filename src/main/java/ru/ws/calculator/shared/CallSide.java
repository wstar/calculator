package ru.ws.calculator.shared;

public enum CallSide {
    PROVIDER, CUSTOMER;
}
