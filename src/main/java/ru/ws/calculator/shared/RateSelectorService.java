package ru.ws.calculator.shared;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

@RemoteServiceRelativePath("RateSelectorService")
public interface RateSelectorService extends RemoteService {
    BigDecimal getRateAt(Date date);

    Map<Date, BigDecimal> getAllRates() throws Exception;

    void addRate(Date date, BigDecimal rate) throws Exception;

    void removeRate(Date date) throws Exception;
}
