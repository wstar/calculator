package ru.ws.calculator.shared;

public enum Forfeit implements DetailsOfViolation {
    ONE_300("1/300", 300),
    ONE_150("1/150", 150),
    ONE_130("1/130", 130),
    ;

    final String title;
    final Integer divider;

    Forfeit(String title, Integer div) {
        this.title = title;
        this.divider = div;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public DetailsOfViolation forValue(String value) {
        return valueOf(value);
    }

    public Integer getDivider() {
        return divider;
    }
}
