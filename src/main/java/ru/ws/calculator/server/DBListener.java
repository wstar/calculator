package ru.ws.calculator.server;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.logging.Logger;

public class DBListener implements ServletContextListener {

    static EntityManagerFactory emf;

    public DBListener() {
    }

    public void contextInitialized(ServletContextEvent sce) {
        if (emf == null) {
            Logger.getLogger("DBListener").info("Start DB session");
            emf = Persistence.createEntityManagerFactory("calculator.rates");
        }
        sce.getServletContext().setAttribute("emf", emf);

        EntityManager em = emf.createEntityManager();
        Long count = (Long) em.createQuery("select count(rv) from RateValue rv").getSingleResult();
        if (count == 0) {
            try {
                em.getTransaction().begin();
                RatesCSVActionsServlet.uploadCsv(getClass().getResourceAsStream("/rates.csv"), em);
            } catch (Exception e) {
                em.getTransaction().rollback();
                Logger.getLogger("DBListener").throwing(this.getClass().getName(), "init", e);
            } finally {
                em.getTransaction().commit();
            }
        }
        em.close();
    }

    public void contextDestroyed(ServletContextEvent sce) {
        if (emf.isOpen()) {
            emf.close();
            emf = null;
        }
    }
}
