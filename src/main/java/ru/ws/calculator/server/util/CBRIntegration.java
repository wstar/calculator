package ru.ws.calculator.server.util;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.soap.*;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

public class CBRIntegration {
    static final Logger LOG = Logger.getLogger(CBRIntegration.class.getName());
    static final String CBR_URL = "https://cbr.ru/DailyInfoWebServ/DailyInfo.asmx";
    static final String ALL_DATA_METHOD = "AllDataInfoXML";

    public static BigDecimal getKeyRateAt(Date date) {
        try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection connection = soapConnectionFactory.createConnection();

            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();

            SOAPPart soapPart = soapMessage.getSOAPPart();

            SOAPEnvelope envelope = soapPart.getEnvelope();
            envelope.addNamespaceDeclaration("", "http://web.cbr.ru/");

            SOAPBody soapBody = envelope.getBody();
            SOAPElement method = soapBody.addChildElement(ALL_DATA_METHOD, "", "http://web.cbr.ru/");
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm:ss");
            String buff = sdf1.format(date) + 'T' + sdf2.format(date);
            method.addChildElement("fromDate").setTextContent(buff);
            method.addChildElement("ToDate").setTextContent(buff);
            method.addChildElement("date").setTextContent(buff);

            MimeHeaders headers = soapMessage.getMimeHeaders();
            headers.addHeader("SOAPAction", "http://web.cbr.ru/" + ALL_DATA_METHOD);

            soapMessage.saveChanges();

            ByteArrayOutputStream baosMain = new ByteArrayOutputStream();
            soapMessage.writeTo(baosMain);
            LOG.info("Request " + new String(baosMain.toByteArray()));

            SOAPMessage response = connection.call(soapMessage, CBR_URL);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            response.writeTo(baos);
            LOG.info(new String(baos.toByteArray()));

            XPathFactory xPathFactory = XPathFactory.newInstance();
            XPathExpression expression = xPathFactory.newXPath().compile("//KEY_RATE");
            NodeList expectedNodes = (NodeList) expression.evaluate(response.getSOAPBody(), XPathConstants.NODESET);
            LOG.info("Elements found " + expectedNodes.getLength());
            for (int i = 0; i < expectedNodes.getLength(); i++) {
                Node node = expectedNodes.item(i);
                if (node instanceof Element) {
                    Element keyRate = (Element) node;
                    return new BigDecimal(keyRate.getAttribute("val"));
                }
            }
            connection.close();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return null;
    }
}
