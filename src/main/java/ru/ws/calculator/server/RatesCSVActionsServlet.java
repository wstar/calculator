package ru.ws.calculator.server;

import ru.ws.calculator.shared.RateValue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@MultipartConfig
public class RatesCSVActionsServlet extends HttpServlet {
    RateSelectorServiceImpl rateSelectorService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        rateSelectorService = new RateSelectorServiceImpl();
        rateSelectorService.init(config);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Map<Date, BigDecimal> rates = rateSelectorService.getAllRates();
            resp.setHeader("Content-Type", "text/csv");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
            DecimalFormat decimalFormat = new DecimalFormat("##.##");
            resp.setHeader("Content-Disposition", "attachment;filename=rates-" + simpleDateFormat.format(new Date()) + ".csv");
            resp.getWriter().println("Date;Rate");
            for (Map.Entry<Date, BigDecimal> rate : rates.entrySet()) {
                String formattedDate = simpleDateFormat.format(rate.getKey());
                String formattedRate = decimalFormat.format(rate.getValue());
                resp.getWriter().println(String.format("%s;%s", formattedDate, formattedRate));
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace(resp.getWriter());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part part = req.getPart("file");
        EntityManagerFactory emf = (EntityManagerFactory) getServletConfig().getServletContext().getAttribute("emf");
        EntityManager em = emf.createEntityManager();
        try (InputStream is = part.getInputStream()) {
            em.getTransaction().begin();
            uploadCsv(is, em);
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
            resp.getOutputStream().write(e.getMessage().getBytes());
        } finally {
            em.getTransaction().commit();
        }
    }

    public static void uploadCsv(InputStream csv, EntityManager em) throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd.MM.yyyy");
        DecimalFormat decimalFormat = new DecimalFormat("##.##");
        BufferedReader reader = new BufferedReader(new InputStreamReader(csv));
        boolean headerDone = false;
        while (!Thread.interrupted() && reader.ready()) {
            String line = reader.readLine();
            if (line.length() > 20) {
                throw new Exception("bad scv file");
            }
            String[] columns = line.split(";");
            if (columns.length - 1 == 1) {
                if (!headerDone && "Date".equals(columns[0])) {
                    headerDone = true;
                    continue;
                }
                RateValue rateValue = new RateValue();
                try {
                    rateValue.setStartDate(new java.sql.Date(simpleDateFormat.parse(columns[0]).getTime()));
                } catch (Throwable e) {
                    rateValue.setStartDate(new java.sql.Date(simpleDateFormat2.parse(columns[0]).getTime()));
                }
                rateValue.setValue(BigDecimal.valueOf(decimalFormat.parse(columns[1]).doubleValue()));
                RateValue persisted = em.find(RateValue.class, rateValue.getStartDate());
                if (persisted == null) {
                    em.persist(rateValue);
                } else {
                    persisted.setValue(rateValue.getValue());
                    em.merge(persisted);
                }
            } else {
                throw new Exception("bad scv file");
            }
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if ("yes".equalsIgnoreCase(req.getHeader("All-Records"))) {
            try {
                rateSelectorService.removeAll();
            } catch (Exception e) {
                e.printStackTrace();
                e.printStackTrace(resp.getWriter());
            }
        }
    }
}
