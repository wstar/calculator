package ru.ws.calculator.server;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import ru.ws.calculator.server.util.TextReplacer;
import ru.ws.calculator.shared.CalculatorEngine;
import ru.ws.calculator.shared.variants.*;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import static java.time.temporal.ChronoUnit.DAYS;

public class TemplatorServlet extends HttpServlet {

    static Logger LOG = Logger.getLogger("TemplatorServlet");
    static private final String wordContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    static private String contentDisposition = "";
    RateSelectorServiceImpl ratesService;
    ThreadLocal<Integer> variantCounter = ThreadLocal.withInitial(() -> new Integer(1));

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ratesService = new RateSelectorServiceImpl();
        ratesService.init(config);
        synchronized (wordContentType) {
            if (contentDisposition.isEmpty()) {
                try {
                    contentDisposition = ("attachment; filename=\""
                            + URLEncoder.encode("Расчет неустойки Закон №44-ФЗ", "utf-8").replace("+", "%20")
                            + ".docx\"").intern();
                } catch (UnsupportedEncodingException e) {
                    throw new ServletException(e);
                }
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (session == null) {
            return;
        }
        List<VariantValue> value = (List<VariantValue>) session.getAttribute("variant");
        if (value == null) {
            return;
        }
        generateDocX(resp, value);
        session.removeAttribute("variant");
    }

    private void generateDocX(HttpServletResponse resp, List<VariantValue> value) {
        try (XWPFDocument template = new XWPFDocument()) {
            for (VariantValue varve : value) {
                deployVariants(varve, template);
            }
            resp.setHeader("Content-Type", wordContentType);
            resp.setHeader("Content-Disposition", contentDisposition);
            template.write(resp.getOutputStream());
            LOG.info("data sent");
        } catch (IOException e) {
            LOG.throwing("", "", e);
        } finally {
            variantCounter.set(1);
        }
    }

    void deployVariants(VariantValue value, XWPFDocument doc) {
        if (value.getVariant6() != null) {
            Variant6 var6 = value.getVariant6();
            setupVariant6(doc, var6);
        }
        if (value.getVariant7() != null) {
            Variant7 var7 = value.getVariant7();
            setupVariant7(doc, var7);
        }
        if (value.getVariant8() != null) {
            Variant8 var8 = value.getVariant8();
            setupVariant8(doc, var8);
        }
        if (value.getVariant9() != null) {
            Variant9 var9 = value.getVariant9();
            setupVariant9(doc, var9);
        }
        if (value.getVariant10() != null) {
            Variant10 var10 = value.getVariant10();
            setupVariant10(doc, var10);
        }
        if (value.getVariant11() != null) {
            Variant11 var11 = value.getVariant11();
            setupVariant11(doc, var11);
        }
        if (value.getVariant12() != null) {
            Variant12 var12 = value.getVariant12();
            setupVariant12(doc, var12);
        }
        if (value.getVariant13() != null) {
            Variant13 var13 = value.getVariant13();
            setupVariant13(doc, var13);
        }
        if (value.getVariant14() != null) {
            Variant14 var14 = value.getVariant14();
            setupVariant14(doc, var14);
        }
    }

    private void setupVariant6(XWPFDocument doc, Variant6 var6) {
        try (XWPFDocument template = new XWPFDocument(getClass().getResourceAsStream("/templates/variant6.docx"))) {
            NumberFormat numberFormat = NumberFormat.getInstance(Locale.getDefault());
            numberFormat.setMinimumFractionDigits(2);
            TextReplacer replacer = new TextReplacer();
            int i = variantCounter.get();
            replacer.replaceInText(template, "${variantNumber}", String.valueOf(i));
            replacer.replaceInText(template, "${contractCost}", numberFormat.format(var6.getContractCost()));
            replacer.replaceInText(template, "${opsz}", numberFormat.format(var6.getRate() * 100));
            replacer.replaceInText(template, "${totalValue}", numberFormat.format(CalculatorEngine.calculateVariant6(var6).doubleValue()));

            addDoc(doc, template);

            variantCounter.set(++i);
        } catch (IOException e) {
            LOG.throwing("Variant6 generator", "generate", e);
        }
    }

    private void setupVariant7(XWPFDocument doc, Variant7 var7) {
        try (XWPFDocument template = new XWPFDocument(getClass().getResourceAsStream("/templates/variant7.docx"))) {
            NumberFormat numberFormat = NumberFormat.getInstance(Locale.getDefault());
            numberFormat.setMinimumFractionDigits(2);
            TextReplacer replacer = new TextReplacer();
            int i = variantCounter.get();
            replacer.replaceInText(template, "${variantNumber}", String.valueOf(i));
            replacer.replaceInText(template, "${nmck}", numberFormat.format(var7.getContractCost()));
            replacer.replaceInText(template, "${ops4}", numberFormat.format(var7.getRate() * 100));
            replacer.replaceInText(template, "${totalValue}", numberFormat.format(CalculatorEngine.calculateVariant7(var7).doubleValue()));

            addDoc(doc, template);

            variantCounter.set(++i);
        } catch (IOException e) {
            LOG.throwing("Variant7 generator", "generate", e);
        }
    }

    private void setupVariant8(XWPFDocument doc, Variant8 var8) {
        try (XWPFDocument template = new XWPFDocument(getClass().getResourceAsStream("/templates/variant8.docx"))) {
            NumberFormat numberFormat = NumberFormat.getInstance(Locale.getDefault());
            numberFormat.setMinimumFractionDigits(2);
            TextReplacer replacer = new TextReplacer();
            int i = variantCounter.get();
            replacer.replaceInText(template, "${variantNumber}", String.valueOf(i));
            replacer.replaceInText(template, "${contactValue}", numberFormat.format(var8.getContractCost()));
            replacer.replaceInText(template, "${totalValue}", numberFormat.format(CalculatorEngine.calculateVariant8(var8).doubleValue()));

            addDoc(doc, template);

            variantCounter.set(++i);
        } catch (IOException e) {
            LOG.throwing("Variant8 generator", "generate", e);
        }
    }

    private void setupVariant9(XWPFDocument doc, Variant9 var9) {
        try (XWPFDocument template = new XWPFDocument(getClass().getResourceAsStream("/templates/variant9.docx"))) {
            NumberFormat numberFormat = NumberFormat.getInstance(Locale.getDefault());
            numberFormat.setMinimumFractionDigits(2);
            TextReplacer replacer = new TextReplacer();
            int i = variantCounter.get();
            replacer.replaceInText(template, "${variantNumber}", String.valueOf(i));
            replacer.replaceInText(template, "${contractCost}", numberFormat.format(var9.getContractCost()));
            replacer.replaceInText(template, "${rate}", numberFormat.format(var9.getRate() * 100));
            replacer.replaceInText(template, "${totalValue}", numberFormat.format(CalculatorEngine.calculateVariant9(var9).doubleValue()));

            addDoc(doc, template);

            variantCounter.set(++i);
        } catch (IOException e) {
            LOG.throwing("Variant9 generator", "generate", e);
        }
    }

    private void setupVariant10(XWPFDocument doc, Variant10 var10) {
        try (XWPFDocument template = new XWPFDocument(getClass().getResourceAsStream("/templates/variant10.docx"))) {
            NumberFormat numberFormat = NumberFormat.getInstance(Locale.getDefault());
            numberFormat.setMinimumFractionDigits(2);
            TextReplacer replacer = new TextReplacer();
            int i = variantCounter.get();
            replacer.replaceInText(template, "${variantNumber}", String.valueOf(i));
            replacer.replaceInText(template, "${cost}", numberFormat.format(var10.getContractCost()));
            replacer.replaceInText(template, "${rate}", numberFormat.format(var10.getRate() * 100));
            replacer.replaceInText(template, "${totalValue}", numberFormat.format(CalculatorEngine.calculateVariant10(var10).doubleValue()));

            addDoc(doc, template);

            variantCounter.set(++i);
        } catch (IOException e) {
            LOG.throwing("Variant10 generator", "generate", e);
        }
    }

    private void setupVariant11(XWPFDocument doc, Variant11 var11) {
        try (XWPFDocument template = new XWPFDocument(getClass().getResourceAsStream("/templates/variant11.docx"))) {
            NumberFormat numberFormat = NumberFormat.getInstance(Locale.getDefault());
            numberFormat.setMinimumFractionDigits(2);
            TextReplacer replacer = new TextReplacer();
            int i = variantCounter.get();
            replacer.replaceInText(template, "${variantNumber}", String.valueOf(i));
            replacer.replaceInText(template, "${cost}", numberFormat.format(var11.getContractCost()));
            replacer.replaceInText(template, "${rate}", numberFormat.format(var11.getRate() * 100));
            replacer.replaceInText(template, "${totalValue}", numberFormat.format(CalculatorEngine.calculateVariant11(var11).doubleValue()));

            addDoc(doc, template);

            variantCounter.set(++i);
        } catch (IOException e) {
            LOG.throwing("Variant11 generator", "generate", e);
        }
    }

    private void setupVariant12(XWPFDocument doc, Variant12 var12) {
        try (XWPFDocument template = new XWPFDocument(getClass().getResourceAsStream("/templates/variant12.docx"))) {
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            NumberFormat numberFormat = NumberFormat.getInstance(Locale.getDefault());
            numberFormat.setMinimumFractionDigits(2);
            Date starPenalty = var12.getStartPenalty();
            Date endPenalty = var12.getEndPenalty();
            TextReplacer replacer = new TextReplacer();
            int i = variantCounter.get();
            replacer.replaceInText(template, "${variantNumber}", String.valueOf(i));
            replacer.replaceInText(template, "${start}", format.format(starPenalty));
            replacer.replaceInText(template, "${end}", format.format(endPenalty));
            long between = DAYS.between(starPenalty.toInstant(), endPenalty.toInstant());
            replacer.replaceInText(template, "${daysGap}", String.valueOf(between));
            replacer.replaceInText(template, "${payDate}", format.format(var12.getPenaltyPayDate()));
            BigDecimal refPay;
            if (var12.getRate() == null) {
                refPay = ratesService.getRateAt(var12.getPenaltyPayDate());
                if (refPay == null) {
                    LOG.warning("ref pay is null");
                    refPay = BigDecimal.ONE;
                }
            } else {
                refPay = BigDecimal.valueOf(var12.getRate());
            }
            replacer.replaceInText(template, "${refPay}", numberFormat.format(refPay.doubleValue()));
            replacer.replaceInText(template, "${contractCost}", numberFormat.format(var12.getContactCost()));
            replacer.replaceInText(template, "${doneCost}", numberFormat.format(var12.getDoneContractCost()));
            replacer.replaceInText(template, "${multiplier}", "1/" + var12.getDivider());

            BigDecimal total = CalculatorEngine.calculateVariant12(var12);
            replacer.replaceInText(template, "${totalValue}", numberFormat.format(total.doubleValue()));

            addDoc(doc, template);

            variantCounter.set(++i);
        } catch (IOException e) {
            LOG.throwing("Variant12 generator", "generate", e);
        }
    }

    private void setupVariant13(XWPFDocument doc, Variant13 var13) {
        try (XWPFDocument template = new XWPFDocument(getClass().getResourceAsStream("/templates/variant13.docx"))) {
            NumberFormat numberFormat = NumberFormat.getInstance(Locale.getDefault());
            numberFormat.setMinimumFractionDigits(2);
            TextReplacer replacer = new TextReplacer();
            int i = variantCounter.get();
            replacer.replaceInText(template, "${variantNumber}", String.valueOf(i));
            replacer.replaceInText(template, "${contractCost}", numberFormat.format(var13.getContractCost()));
            replacer.replaceInText(template, "${totalValue}", numberFormat.format(CalculatorEngine.calculateVariant13(var13).doubleValue()));

            addDoc(doc, template);

            variantCounter.set(++i);
        } catch (IOException e) {
            LOG.throwing("Variant13 generator", "generate", e);
        }
    }

    private void setupVariant14(XWPFDocument doc, Variant14 var14) {
        try (XWPFDocument template = new XWPFDocument(getClass().getResourceAsStream("/templates/variant14.docx"))) {
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            NumberFormat numberFormat = NumberFormat.getInstance(Locale.getDefault());
            numberFormat.setMinimumFractionDigits(2);
            Date starPenalty = var14.getStartPenalty();
            Date endPenalty = var14.getEndPenalty();
            TextReplacer replacer = new TextReplacer();
            int i = variantCounter.get();
            BigDecimal refPay;
            if (var14.getRate() == null) {
                refPay = ratesService.getRateAt(var14.getPenaltyPayDate());
                if (refPay == null) {
                    LOG.warning("ref pay is null");
                    refPay = BigDecimal.ONE;
                }
            } else {
                refPay = BigDecimal.valueOf(var14.getRate());
            }
            replacer.replaceInText(template, "${variantNumber}", String.valueOf(i));
            replacer.replaceInText(template, "${start}", format.format(starPenalty));
            replacer.replaceInText(template, "${end}", format.format(endPenalty));
            long between = DAYS.between(starPenalty.toInstant(), endPenalty.toInstant());
            replacer.replaceInText(template, "${daysGap}", String.valueOf(between));
            replacer.replaceInText(template, "${payDate}", format.format(var14.getPenaltyPayDate()));
            replacer.replaceInText(template, "${refPay}", numberFormat.format(refPay.doubleValue()));
            replacer.replaceInText(template, "${contractCost}", numberFormat.format(var14.getContactCost()));
            replacer.replaceInText(template, "${doneCost}", numberFormat.format(var14.getDoneContractCost()));
            replacer.replaceInText(template, "${multiplier}", "1/" + var14.getDivider());

            BigDecimal total = CalculatorEngine.calculateVariant14(var14);
            replacer.replaceInText(template, "${totalValue}", numberFormat.format(total.doubleValue()));

            addDoc(doc, template);

            variantCounter.set(++i);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void addDoc(XWPFDocument doc, XWPFDocument template) {
        for (XWPFParagraph para : template.getParagraphs()) {
            if (!para.getParagraphText().isEmpty()) {
                XWPFParagraph newpara = doc.createParagraph();
                copyAllRunsToAnotherParagraph(para, newpara);
            }
        }

        doc.createParagraph();
        doc.createParagraph();
//        XWPFRun breakRun = pageBreack.createRun();
//        breakRun.addBreak(BreakType.PAGE);
    }

    private static void copyAllRunsToAnotherParagraph(XWPFParagraph oldPar, XWPFParagraph newPar) {
        final int DEFAULT_FONT_SIZE = 10;

        for (XWPFRun run : oldPar.getRuns()) {
            String textInRun = run.getText(0);
            if (textInRun == null || textInRun.isEmpty()) {
                continue;
            }

            int fontSize = run.getFontSize();

            XWPFRun newRun = newPar.createRun();

            // Copy text
            newRun.setText(textInRun);

            // Apply the same style
            newRun.setFontSize((fontSize == -1) ? DEFAULT_FONT_SIZE : run.getFontSize());
            newRun.setFontFamily(run.getFontFamily());
            newRun.setBold(run.isBold());
            newRun.setItalic(run.isItalic());
            newRun.setStrike(run.isStrike());
            newRun.setColor(run.getColor());
        }
    }
}
