package ru.ws.calculator.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ru.ws.calculator.shared.TemplateCreatorService;
import ru.ws.calculator.shared.variants.VariantValue;

import javax.servlet.http.HttpSession;
import java.util.List;

public class TemplateCreatorServiceImpl extends RemoteServiceServlet implements TemplateCreatorService {
    @Override
    public String putData(List<VariantValue> value) {
        HttpSession session = getThreadLocalRequest().getSession(true);
        session.setAttribute("variant", value);
        return session.getId();
    }
}