package ru.ws.calculator.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import ru.ws.calculator.shared.RateSelectorService;
import ru.ws.calculator.shared.RateValue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

public class RateSelectorServiceImpl extends RemoteServiceServlet implements RateSelectorService {
    static Logger LOG = Logger.getLogger("RateServlet");
    private EntityManagerFactory emf;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        emf = (EntityManagerFactory) config.getServletContext().getAttribute("emf");
    }

    public static BigDecimal selectRate(EntityManager em, Date date, boolean equal) {
        RateValue rateValue = em.find(RateValue.class, date);
        em.close();
        if (rateValue == null) {
            return null;
        }
        return rateValue.getValue();
    }

    @Override
    public BigDecimal getRateAt(Date date) {
        EntityManager entityManager = emf.createEntityManager();
        TypedQuery<RateValue> query = entityManager.createQuery(
                "select rv from RateValue rv where rv.startDate <= :start_date order by rv.startDate desc",
                RateValue.class
        );
        query.setParameter("start_date", new java.sql.Date(date.getTime()));
        RateValue rate = query.setMaxResults(1).getSingleResult();
        entityManager.close();
        if (rate == null) {
            return null;
        }
        return rate.getValue();
    }

    @Override
    public Map<Date, BigDecimal> getAllRates() throws Exception {
        LinkedHashMap<Date, BigDecimal> result = new LinkedHashMap<>();
        EntityManager entityManager = emf.createEntityManager();
        entityManager
                .createQuery("select rv from RateValue rv order by rv.startDate desc", RateValue.class)
                .getResultList()
                .forEach(e -> result.put(e.getStartDate(), e.getValue()));
        entityManager.close();
        return result;
    }

    @Override
    public void addRate(Date date, BigDecimal rate) throws Exception {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        RateValue rateValue = entityManager.find(RateValue.class, date);
        if (rateValue == null) {
            rateValue = new RateValue();
            rateValue.setStartDate(new java.sql.Date(date.getTime()));
            LOG.info("add new rate " + date + " " + rate);
        } else {
            LOG.info("update rate " + date + " " + rate);
        }
        rateValue.setValue(rate);
        entityManager.persist(rateValue);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeRate(Date date) throws Exception {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        RateValue rateValue = entityManager.find(RateValue.class, date);
        entityManager.remove(rateValue);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void removeAll() throws Exception {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        Query query = entityManager.createQuery("delete from RateValue");
        query.executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}