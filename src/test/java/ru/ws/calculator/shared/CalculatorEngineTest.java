package ru.ws.calculator.shared;

import com.google.gwt.user.datepicker.client.CalendarUtil;
import org.junit.Test;
import ru.ws.calculator.shared.variants.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class CalculatorEngineTest {

    @Test
    public void calculateVariant6() {
        Variant6 var6 = new Variant6();
        var6.setContractCost(100.);
        assertEquals(CalculatorEngine.calculateVariant6(var6), BigDecimal.valueOf(1.00).setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    public void calculateVariant7() {
        Variant7 var7 = new Variant7();
        var7.setContractCost(100.);
        assertEquals(CalculatorEngine.calculateVariant7(var7), BigDecimal.valueOf(5.0).setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    public void calculateVariant8() {
        Variant8 var8 = new Variant8();
        var8.setContractCost(100.);
        assertEquals(CalculatorEngine.calculateVariant8(var8), BigDecimal.valueOf(100.0).setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    public void calculateVariant9() {
        Variant9 var9 = new Variant9();
        var9.setContractCost(100.);
        assertEquals(CalculatorEngine.calculateVariant9(var9), BigDecimal.valueOf(5.0).setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    public void calculateVariant10() {
        Variant10 var10 = new Variant10();
        var10.setContractCost(100.);
        assertEquals(CalculatorEngine.calculateVariant10(var10), BigDecimal.valueOf(5.0).setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    public void calculateVariant11() {
        Variant11 var11 = new Variant11();
        var11.setContractCost(100.);
        assertEquals(CalculatorEngine.calculateVariant11(var11), BigDecimal.valueOf(5.0).setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    public void calculateVariant12() {
        Variant12 var12 = new Variant12();
        var12.setDoneContractCost(100.);
        var12.setContactCost(1000.);
        var12.setDivider(150);
        Date start = new Date();
        var12.setStartPenalty(start);
        CalendarUtil.addDaysToDate(start, 30);
        var12.setEndPenalty(start);
        var12.setRate(6.);
        assertEquals(CalculatorEngine.calculateVariant12(var12), BigDecimal.valueOf(0.72).setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    public void calculateVariant13() {
        Variant13 var13 = new Variant13();
        var13.setContractCost(100.);
        assertEquals(CalculatorEngine.calculateVariant13(var13), BigDecimal.valueOf(100.0).setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    public void calculateVariant14() {
        Variant14 var14 = new Variant14();
        var14.setDoneContractCost(100.);
        var14.setContactCost(1000.);
        var14.setDivider(150);
        Date start = new Date();
        var14.setStartPenalty(start);
        CalendarUtil.addDaysToDate(start, 30);
        var14.setEndPenalty(start);
        var14.setRate(6.);
        assertEquals(CalculatorEngine.calculateVariant14(var14), BigDecimal.valueOf(0.72).setScale(2, RoundingMode.HALF_UP));
    }
}