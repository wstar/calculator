package ru.ws.calculator.server;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import ru.ws.calculator.shared.variants.Variant6;
import ru.ws.calculator.shared.variants.VariantValue;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Logger;

import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class DocxGenerationTest {
    static final Logger LOG = Logger.getLogger(DocxGenerationTest.class.getName());
    TemplatorServlet servlet;
    String data;
    byte[] docx;
    @Before
    public void init() {
        servlet = new TemplatorServlet();
    }

    @Test
    public void testGeneration() throws ServletException, IOException {
        VariantValue src = new VariantValue();
        Variant6 variant6 = new Variant6();
        variant6.setContractCost(123.45);
        src.setVariant6(variant6);
        HttpSession session = Mockito.mock(HttpSession.class);
        Mockito.when(session.getAttribute("variant")).thenReturn(src);
        HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse resp = Mockito.mock(HttpServletResponse.class);
        ServletOutputStream stream = new ServletOutputStream() {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            @Override
            public boolean isReady() {
                return stream != null;
            }

            @Override
            public void setWriteListener(WriteListener writeListener) {

            }

            @Override
            public void write(int b) throws IOException {
                stream.write(b);
            }

            @Override
            public void flush() throws IOException {
                super.flush();
                stream.flush();
                docx = stream.toByteArray();
            }

            @Override
            public void close() throws IOException {
                super.close();
                stream.close();
                docx = stream.toByteArray();
            }
        };
        Mockito.when(resp.getOutputStream()).thenReturn(stream);
        servlet.doGet(req, resp);
        stream.close();
        assertNotNull(docx);
    }
}